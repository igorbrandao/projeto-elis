#include <iostream>
#include <fstream>

using namespace std;

template <typename F>
class fileHandler
{
	private:
		ofstream outputFile;

	public:

		/*!
	     * Functions definition
		*/

		/********************************************//**
		*  ... text
		***********************************************/
		bool isOpen() const
		{
			return ( this->outputFile.is_open() );
		}

		/********************************************//**
		*  ... text
		***********************************************/
	    void openFile( F const& fileName ) const
	    {
    		this->outputFile.open( fileName );
	    }

	    /********************************************//**
		*  ... text
		***********************************************/
		void closeFile() const
		{
			this->outputFile.close();
		}

		/********************************************//**
		*  ... text
		***********************************************/
		inline void printFile()
		{
			string line;
			while( !this->outputFile.eof() )
			{
				getline( this->outputFile, line );
				cout << line << endl;
			}
			this->closeFile( this->outputFile );
		}

		/********************************************//**
		*  ... text
		***********************************************/
		inline void saveFile( F const& content_ )
		{
			this->outputFile << content_ << endl;
		}
};