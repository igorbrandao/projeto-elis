//este é o cabeçalho da parte de manipulação de arquivos.
/*
#include <string>
#include "sllist.h"
#ifndef HANDLESFILE_H_
#define HANDLESFILE_H_

#include "sllist.h"

using namespace std;

void save(string _fileName, list<string> & _myList);
*/
#ifndef HANDLESFILE_H_
#define HANDLESFILE_H_

#include <list>
#include <fstream>
#include <iterator>
#include <algorithm>

template< typename T, typename A >
void write_to_file_1( const std::list<T,A>& seq, const char* path2file )
{
    std::ofstream file(path2file) ;
    for( const auto& v : seq ) file << v << '\n' ;
}

#endif