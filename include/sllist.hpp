/*! \sllist.cpp
 * Brief description: Implements list class functions.
 *
*/

/********************************************//**
* \return the begin of the list
***********************************************/
template <typename T>
inline typename list<T>::iterator list<T>::begin()
{
    return iterator(_head);
}

/********************************************//**
* \return the end of the list
***********************************************/
template <typename T>
inline typename list<T>::iterator list<T>::end()
{
    return iterator(NULL);
}

/********************************************//**
* \return the begin of the list
***********************************************/
template <typename T>
inline typename list<T>::const_iterator list<T>::begin() const
{
    return const_iterator(_head);
}

/********************************************//**
* \return the end of the list
***********************************************/
template <typename T>
inline typename list<T>::const_iterator list<T>::end() const
{
    return const_iterator(NULL);
}

/********************************************//**
 *  ... text
 ***********************************************/
template <typename T>
inline void list<T>::insert_after(typename list<T>::iterator where, T const& elt)
{
    node* newnode = new node(elt);
    newnode->next = where._curpos->next;
    where._curpos->next = newnode;
    _size++;
    if (newnode->next == NULL)
    {
        _tail = newnode;
    }
}

/********************************************//**
* Inserts an element at the first node of the list
***********************************************/
template <typename T>
inline void list<T>::push_front(T const& elt)
{
    node* newnode = new node(elt);
    newnode->next = _head;
    _head = newnode;
    if (_tail == NULL)
    {
        _tail = newnode;
    }
    _size++;
}

/********************************************//**
* Inserts an element at the last node of the list
***********************************************/
template <typename T>
inline void list<T>::push_back(T const& elt)
{
    if (_tail != NULL)
        insert_after(iterator(_tail), elt);
    else
        push_front(elt);
}

/********************************************//**
 *  ... text
 ***********************************************/
template <typename T>
inline typename list<T>::iterator list<T>::erase_after(typename list<T>::iterator it)
{
    node* todelete = it._curpos->next;
    it._curpos->next = it._curpos->next->next;
    _size--;

    if (it._curpos->next == NULL)
    {
        _tail = it._curpos;
    }
    delete todelete;
    return iterator(it._curpos->next);
}

/********************************************//**
* Removes the first element of the list
***********************************************/
template <typename T>
inline void list<T>::pop_front()
{
    node* todelete = _head;
    _head = _head->next;
    if (_head == NULL) {
        _tail = NULL;
    }
    _size--;
    delete todelete;
}

/********************************************//**
* Clear all elements of the list
***********************************************/
template <typename T>
inline void list<T>::clear()
{
    while (begin() != end())
    {
        pop_front();
    }
}

/********************************************//**
 *  ... text
 ***********************************************/
template <typename T>
list<T>& list<T>::operator=(list<T> const& rhs)
{
    clear();

    const_iterator rhs_it = rhs.begin();
    for (rhs_it = rhs.begin(); rhs_it != rhs.end(); ++rhs_it)
    {
        push_back(*rhs_it);
    }

    return *this;
}

/********************************************//**
* This function returns the size of the list
* \return the size of the list
***********************************************/
template <typename T>
inline size_t list<T>::size() const
{
  return _size;
}

/********************************************//**
* Checks if the list is empty
* \return true if the size of the list is zero
***********************************************/
template <typename T>
inline bool list<T>::empty() const
{
  return _size == 0;
}

/********************************************//**
* Constructor of the list class
***********************************************/
template <typename T>
inline list<T>::list()
  : _head(NULL), _tail(NULL), _size(0)
{}

/********************************************//**
* Overload of the list class constructor
***********************************************/
template <typename T>
inline list<T>::list(list<T> const& rhs)
  : _head(NULL), _tail(NULL), _size(0)
{
    *this = rhs;
}

/********************************************//**
* Deconstructor of the list class
***********************************************/
template <typename T>
inline list<T>::~list()
{
    clear();
}

/********************************************//**
* Prints the contents stored in the list
***********************************************/
template <typename T>
void print_list(list<T> const& rhs)
{
  typename list<T>::const_iterator it;
  for (it = rhs.begin(); it != rhs.end(); ++it)
    std::cout << *it << "\n";
  std::cout << std::endl;
}

/********************************************//**
* Returns a string that is stored in an external file 
* \param rhs the list that stores the content entered by the user
* \return the string to be stored in external file
***********************************************/
template <typename T>
std::string get_list(list<T> const& rhs)
{
    string result = "";
  typename list<T>::const_iterator it;
  for (it = rhs.begin(); it != rhs.end(); ++it)
    result +=  *it + "\n";
  return result;
}