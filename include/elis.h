/*! \sllist.h
 * Brief description: Class to manage Elis.
 *
*/
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <ncurses.h>
#include <iterator>

#include "sllist.h"
#include "fileHandler.h"

using std::string;

template <typename T>
class elis
{
	private:

		/*! 
	     * Class attributes
		*/
		string fileName = "test";				/** Define the output filename. */
	    string str = "";                        /** This string will keep the content to be stored in list node. */

	    list<string> myList;                    /** List instance */
	    int status = 1;                         /** Status  */
	    int lineCounter = 1;					/** Store the coordinates of the cursor in the screen*/
	    int row = 0 ,col = 0;
	    int rowScreen = 0, colScreen = 0;       /** To store the number of rows and the number of colums of the screen */
	    char ch = 0;                            /** Receives the character that will be concatenated in the string. */

  	public:

	    /*!
	     * Functions definition
		*/
	    //explicit elis(); // constructor
	    //elis( list<T> const& rhs ); // constructor overload
	    //~elis(); // destructor

	    void start(); // initialize the program
	    void keypress( T const& val ); // check the pressed key
		void keypressEnter(); // keypress Enter
		void keypressBackspace(); // keypress Backspace
		void keypressEsc(); // keypress Esc
		void keypressAny(); // handle the other keys

		void commandW( T const& val ); // command W [<name>]
		void commandE( T const& val ); // command E [<name>]
		void commandI( T const& val ); // command I [n]
		void commandA( T const& val ); // command A [n]
		void commandM( T const& val ); // command M [n]
		void commandD( T const& val, T const& val2 ); // command D [n [m]]
		void commandL( T const& val, T const& val2 ); // command L [n [m]]
		void commandH(); // command H
		void commandQ(); // command Q

		void manageScreen(); // manage graphical elements

		void returnFirstChar( char *val ); // return the first command char
};

#include "elis.hpp"