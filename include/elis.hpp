/*! \sllist.cpp
 * Brief description: Implements elis class functions.
 *
*/

/********************************************//**
* Starts the elis editor
* This function starts the elis editor in a ncurses mode 
***********************************************/
template <typename T>
inline void elis<T>::start()
{
    list<string> myList;
    initscr();                                  /** Starts ncurses mode */
    getmaxyx( stdscr, this->rowScreen, this->colScreen) ;   /** Get the number of this->rows and this->columns */
    keypad( stdscr, true );                        /** This function is necessary to get the hot keys on right ascII code */
    noecho();                                   /** Prevents the typed character appears on the screen */
    printw( "%d> ", this->lineCounter );                      /** Prints the first number line */

    while( true )
    {
        /** Capture and check the key pressed */
        this->ch = getch();
        this->keypress( this->ch );
    }
}

/********************************************//**
* Checks which key was entered
* This function check which key was entered and redirects to their specific function
* \param key the key that the user pressed
***********************************************/
template <typename T>
inline void elis<T>::keypress( T const& key )
{
    switch( key )
    {
        /** If the User typed ENTER */
        case 10: this->keypressEnter(); break;

        /** If the User typed BACKSPACE */
        case 7: this->keypressBackspace(); break;

        /** If the User typed KEY_ESC */
        case 27: this->keypressEsc(); break;

        /** Handle the other keys */
        default: this->keypressAny(); break;
    }
}

/********************************************//**
* Stores the line entered in a list node.
***********************************************/
template <typename T>
inline void elis<T>::keypressEnter()
{
    /** Update the line counter */
    this->lineCounter += 1;

    /** Print the number of the line */
    printw( "\n%d> ", this->lineCounter );

    /** Store the string on a node of the list */
    myList.push_back( this->str );

    /** clean the string */
    this->str = "";
}

/********************************************//**
* Deletes the last entered character and moves the cursor one column left
***********************************************/
template <typename T>
inline void elis<T>::keypressBackspace()
{
    /** Gets the cursor position on the screen */ 
    getyx( stdscr, this->row, this->col );

    /** Test with string is empty */
    if( this->str != "" )
    {
        /** Erase the last caracter in the str string */
        this->str.erase( this->str.end()-1 );

        /** Move the cursor to coordinates x,y */
        move( this->row, (this->col-1) );

        /** Deletes a char */
        delch();

        /** Refresh the screen */
        refresh();
    }
}

/********************************************//**
*  ... text
***********************************************/
template <typename T>
inline void elis<T>::keypressEsc()
{
    /** Catch the whole line */    
    char* command = new char[255];
    getnstr( command, ( sizeof( command ) - 1 ) );
    this->returnFirstChar( command );
    printw( "\n%sXXX ", command );

    /** Check if it's an ESC_KEY */
    /*while( this->ch != 27 )
    {
        if( this->returnFirstChar( this->ch ) == 'w')
        {
            cout << "Eu vou salvar essa baga�a" << endl;
            //write_to_file_1( myList, path2file );

            ofstream outputFile;
            outputFile.open( nomeDoArquivo + ".txt" );
            outputFile << get_list(myList) << endl;
            outputFile.close();
        }
        //this->ch = getch();
    }*/

    //save( nomeDoArquivo, myList);
    
    
}

/********************************************//**
* Concatenate the letters typed by the user in a string
* This function concatenates the characters typed by the user 
* in a string that is stored in the node of the list. and also prints in the corner the cursor position.
***********************************************/
template <typename T>
inline void elis<T>::keypressAny()
{
    /** Prints the key pressed by the User */
    printw( "%c", this->ch );

    /** get the cursor position */
    getyx( stdscr, this->row, this->col );

    /** Print the position of the cursor on the end of the screen */
    mvprintw( (this->rowScreen - 1), (this->colScreen - 24), "cursor position( %d, %d )", (this->row+1), (this->col-3) );

    /** Return the cursor to the last position. */
    move( this->row, this->col );

    /** Dynamically refresh screen */
    refresh();

    /** Fills the string concatenating with the typed characters */
    this->str = this->str + this->ch;
}

/********************************************//**
* Takes the first character entered in command mode
* \param s the first char entered in command mode by the User
***********************************************/
template <typename T>
inline void elis<T>::returnFirstChar( char *s )
{
    if (*s == '\0') return;
    *s = *(s+1);
    this->returnFirstChar( s+1 );
}