#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <ncurses.h>
#include <iterator>
#include "sllist.h"


using namespace std;

void sair();

int main()
{
    /** DELCARACAO DE VARIAVEIS*/
    string nomeDoArquivo = "teste";
    string saveStr = "";
    list<string> myList;                    /** Store the number of the line */
    int status = 1;                         /** The list will store the string data */
    int i = 1;                              /** Store the coordinates of the cursor in the screen*/
    int row = 0 ,col = 0;
    int rowScreen = 0, colScreen = 0;       /** To store the number of rows and the number of colums of the screen */
    char ch = 0;                            /** Receives the character that will be concatenated in the string. */
    string str = "";                        /** The string is stored in the node of the list. */

    initscr();                              /** Starts ncurses mode */
    getmaxyx(stdscr,rowScreen,colScreen);   /** get the number of rows and columns */
    keypad(stdscr,true);                    /** This function is necessary to get the hot keys on right ascII code */
    noecho();                               /** prevents the typed character appears on the screen */
    printw("%d> ", i);                      /** Prints the first number line */

    while(status)
    {
        
        ch = getch();
        switch(ch)
        {
            /** If the User typed ENTER */
            case 10:
                /** Update the counter line */
                i++;
                /** Print the number of the line */
                printw("\n%d> ", i); 
                /** Store the string on a node of the list */
                myList.push_back(str);
                /** clean the string */
                str = "";
                break;
            /** If the User typed BACKSPACE */
            case 7:
                /** Gets the cursor position on the screen */ 
                getyx(stdscr, row, col);
                //primeiro vamos tratar se a string n�o � vazia.
                if( str != "")
                {   /** Erase the last caracter in the str string */
                    str.erase( str.end()-1);
                    /** Move the cursor to coordinates x,y */
                    move(row,col-1);
                    /** Deletes a char */
                    delch();
                    /** Refresh the screen */
                    refresh();
                }
                break;
            /** If the User typed KEY_ESC */
            case 27:
                ch = getch();
                while(ch != 27)
                {
                    if( ch == 'w')
                    {
                        cout << "Eu vou salvar essa baga�a" << endl;
                        //write_to_file_1( myList, path2file );

                        ofstream outputFile;
                        outputFile.open( nomeDoArquivo + ".txt" );
                        outputFile << get_list(myList) << endl;
                        outputFile.close();
                    }
                    ch = getch();
                }

                //save( nomeDoArquivo, myList);
                
                printw("\nDo you want to EXIT the program? (Y/N)");
                ch = getch();
                if(ch == 121 || ch == 89)
                {   
                    status = 0;
                     /** Store the string on a node of the list */
                    myList.push_back(str);
                    break;
                }
                else
                    break;
            default:
                /** Prints the key pressed by the User  */
                printw( "%c", ch );
                /** get the cursor position */
                getyx(stdscr, row, col);
                /** Print the position of the cursor on the end of the screen */
                mvprintw(rowScreen-1,colScreen-24,"cursor position( %d, %d)",row+1, col-3);
                /** Return the cursor to the last position. */
                move(row,col);
                /** I dont know yet what this function mades exactly */
                refresh();
                /** Fills the string concatenating with the typed characters    */
                str = str+ch;
                
                break;
        }
    }
    /** Ends the ncurses mode*/
    endwin();
    /** Print the elements of myList*/
    print_list(myList);
    return 0;
}
/*
void sair(list MyList)
{
    endwin();
    print_list(MyList);
    system("pause");
    exit(0);
}
*/
