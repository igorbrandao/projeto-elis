# Makefile for building Elis application.
# by Leandro Antonio Feliciano da Silva & Igor Augusto Brandao
#
TARGETS=drive_list

CROSS_TOOL =
CC_CPP = $(CROSS_TOOL)g++

CFLAGS = -Wall -g -std=c++0x

all: clean $(TARGETS)

$(TARGETS):
	$(CC_CPP) $(CFLAGS) -I include main.cpp  -lncurses -o main

clean:
	rm -f $(TARGETS)