/*! \drive_list.cpp
 * Brief description: Drive to test linked list class.
 *
 * Detailed description: Creates 2 linked list and call functions
 * from list class.
*/
#include <iostream>
#include "sllist.h"

int main()
{
    //Testando a lista.
    list<int> lista1;

    for (int i = 0; i < 10; ++i)
    {
      lista1.push_front(i);
    }

    //Imprimindo a lista.
    print_list(lista1);
    std::cout << "push_front OK" << std::endl;

    list<int> lista2;
    for (int i = 0; i < 10; ++i)
    {
      lista2.push_back(i+1);
    }    

    //Imprimindo a lista2.
    print_list(lista2);
    std::cout << "push_back OK." << std::endl;

  list<int> lst;
  lst.push_back(2);
  lst.push_back(3);
  lst.push_front(1);
  lst.push_back(4);
  list<int> list2;
  list2 = lst;
  print_list(lst);
  list<int>::iterator it;
  it = lst.begin();
  it++;
  lst.insert_after(it, 42);
  ++it;
  print_list(lst);
  lst.erase_after(++it);
  print_list(lst);
  it = lst.begin();
  *it = 10;
  print_list(lst);
  lst.clear();
  std::cout << "Imprimindo a lista 1" << std::endl;
  print_list(lst);
  std::cout << "Imprimindo a lista 2" << std::endl;
  print_list(list2);

  std::fill(list2.begin(), list2.end(), 42);
  std::cout << "Imprimindo a lista 2 concatenada" << std::endl;
    print_list(list2);

    list<char> lst4char;

    lst4char.push_back('a');
    print_list(lst4char);
    std::cout << "O tamanho da lista de char eh: " << lst4char.size() << std::endl;

    lst4char.push_back('b');
    print_list(lst4char);
    std::cout << "O tamanho da lista de char eh: " << lst4char.size() << std::endl;
  return 0;
}
